<?php

use app\figures\Pawn;

class PawnTest extends \Codeception\Test\Unit
{
    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $figure;

    protected function _before()
    {
    }

    protected function _after()
    {
        $this->figure = null;
    }

    public function testGetName()
    {
        verify("Get name return string 'Pawn'", Pawn::getName())->equals('Pawn');
    }

    public function testValidateMove()
    {
        $this->figure = new Pawn(5, 5);

        $this->specify('Move by one step on X is allowed', function() {
            verify($this->figure->validateMove(4, 5))->true();
            verify($this->figure->validateMove(6, 5))->true();
        });

        $this->specify('Move by one step on Y is allowed', function() {
            verify($this->figure->validateMove(5, 6))->true();
            verify($this->figure->validateMove(5, 4))->true();
        });

        $this->specify('Move by one step on X and Y simultaneously is allowed', function() {
            verify($this->figure->validateMove(4, 4))->true();
            verify($this->figure->validateMove(6, 6))->true();
            verify($this->figure->validateMove(4, 6))->true();
            verify($this->figure->validateMove(6, 4))->true();
        });

        $this->specify('Move by more then one step on X and/or Y is disallowed', function() {
            verify($this->figure->validateMove(3, 5))->false();
            verify($this->figure->validateMove(5, 3))->false();
            verify($this->figure->validateMove(3, 3))->false();
            verify($this->figure->validateMove(7, 5))->false();
            verify($this->figure->validateMove(7, 8))->false();
            verify($this->figure->validateMove(4, 7))->false();
            verify($this->figure->validateMove(7, 4))->false();
        });
    }
}