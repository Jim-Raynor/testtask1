<?php

use app\figures\Queen;

class QueenTest extends \Codeception\Test\Unit
{
    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $figure;

    protected function _before()
    {
    }

    protected function _after()
    {
        $this->figure = null;
    }

    public function testGetName()
    {
        verify("Get name return string 'Queen'", Queen::getName())->equals('Queen');
    }

    public function testValidateMove()
    {
        $this->figure = new Queen(5, 5);

        $this->specify('Any move is allowed for queen', function() {
            verify($this->figure->validateMove(4, 5))->true();
            verify($this->figure->validateMove(6, 5))->true();
            verify($this->figure->validateMove(5, 6))->true();
            verify($this->figure->validateMove(5, 4))->true();
            verify($this->figure->validateMove(4, 4))->true();
            verify($this->figure->validateMove(6, 6))->true();
            verify($this->figure->validateMove(4, 6))->true();
            verify($this->figure->validateMove(6, 4))->true();
            verify($this->figure->validateMove(3, 5))->true();
            verify($this->figure->validateMove(5, 3))->true();
            verify($this->figure->validateMove(3, 3))->true();
            verify($this->figure->validateMove(7, 5))->true();
            verify($this->figure->validateMove(7, 8))->true();
            verify($this->figure->validateMove(4, 7))->true();
            verify($this->figure->validateMove(7, 4))->true();
            verify($this->figure->validateMove(10, 10))->true();
        });
    }
}