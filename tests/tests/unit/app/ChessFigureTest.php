<?php

use Codeception\Util\Stub;

class ChessFigureTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    /**
     * @before testMoveWithValidCoords
     */
    public function testConstructorParams()
    {
        $x = 2;
        $y = 3;
        $figure = Stub::construct(
            'app\ChessFigure',
            ['x' => $x, 'y' => $y],
            [
                'getName' => function() {
                    return '';
                },
                'validateMove' => function($x, $y) {
                    return true;
                },
            ]
        );

        $figureX = $figure->getX();
        $figureY = $figure->getY();

        verify('Coordinates "x" must be the same as passed in constructor', $figureX)->equals($x);
        verify('Coordinates "y" must be the same as passed in constructor', $figureY)->equals($y);
    }

    public function testMoveWithValidCoordinates()
    {
        $figure = Stub::make(
            'app\ChessFigure',
            [
                'getName' => function() {
                    return '';
                },
                'validateMove' => function($x, $y) {
                    return true;
                },
            ]
        );

        $x = 10;
        $y = 3;

        $result = $figure->move($x, $y);
        $figureX = $figure->getX();
        $figureY = $figure->getY();

        verify('Returned value must be TRUE', $result)->true();
        verify('Coordinates "x" must be equal to the set', $figureX)->equals($x);
        verify('Coordinates "y" must be equal to the set', $figureY)->equals($y);
    }

    public function testMoveWithWrongCoordinates()
    {
        $x = 2;
        $y = 3;
        $figure = Stub::construct(
            'app\ChessFigure',
            ['x' => $x, 'y' => $y],
            [
                'getName' => function() {
                    return '';
                },
                'validateMove' => function($x, $y) {
                    return false;
                },
            ]
        );

        $result = $figure->move(10, 7);
        $figureX = $figure->getX();
        $figureY = $figure->getY();

        verify('Returned value must be FALSE', $result)->false();
        verify('Coordinates "x" must be equal to the initial', $figureX)->equals($x);
        verify('Coordinates "y" must be equal to the initial', $figureY)->equals($y);
    }
}