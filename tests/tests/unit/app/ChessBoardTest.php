<?php

use app\ChessBoard;
use app\figures\Pawn;
use app\ChessBoardException;
use app\ConfigException;
use Codeception\Util\Stub;

class ChessBoardTest extends \Codeception\Test\Unit
{
    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;
    private $board;
    private $figure;
    private $figure2;

    protected function _before()
    {
        $this->board = null;
        $this->figure = null;
        $this->figure2 = null;
    }

    protected function _after()
    {
    }

    /**
     * @before testGetFigure
     * @before testGetFigures
     * @before testSetBoardSizes
     * @before testCheckBounds
     * @before testMementoCreateAndLoad
     */
    public function testInitialState()
    {
        $width = 10;
        $height = 9;
        $board = new ChessBoard($width, $height);

        verify('Width the same as set', $board->getWidth())->equals($width);
        verify('Height the same as set', $board->getHeight())->equals($height);
        verify('Figures collection is empty', $board->getFigures())->isEmpty();
        verify('Figures collection is array', $board->getFigures())->internalType('array');
    }

    /**
     * @before testAddFigure
     */
    public function testGetFigure()
    {
        $figure1 = new Pawn(1, 1);
        $figure2 = new Pawn(2, 2);

        $board = Stub::construct(
            'app\ChessBoard',
            ['width' => 10, 'height' => 10],
            [
                'figures' => [
                    'pawn1' => $figure1,
                    3 => $figure2,
                ],
            ]
        );

        verify("Figure returned by key 'pawn1' the same, as added", $board->getFigure('pawn1'))->same($figure1);
        verify("Figure returned by key '3' the same, as added", $board->getFigure(3))->same($figure2);
        verify("Nonexistent key return NULL", $board->getFigure(0))->null();
    }

    /**
     * @before testAddFigure
     */
    public function testGetFigures()
    {
        $figures = [
            new Pawn(1, 1),
            'pawn2' => new Pawn(2, 2)
        ];

        $board = Stub::construct(
            'app\ChessBoard',
            ['width' => 10, 'height' => 10],
            ['figures' => $figures]
        );

        verify("Returned figures collection is equal to original", $board->getFigures())->equals($figures);
    }

    /**
     * @before testRemoveFigures
     * @before testMoveFigure
     */
    public function testAddFigure()
    {
        $this->board = new ChessBoard(10, 10);
        $this->figure = new Pawn(1, 1);
        $this->figure2 = new Pawn(2, 2);

        $this->specify('Simple add figure', function() {
            $this->board->addFigure($this->figure);
            verify('Board contains added figure', $this->board->getFigures())->contains($this->figure);
            verify('Added figure is at key 0', $this->board->getFigure(0))->same($this->figure);
        });

        $this->specify('Add figure with key', function() {
            $key = 'pawn';
            $this->board->addFigure($this->figure, $key);
            verify('Board contains added figure', $this->board->getFigures())->contains($this->figure);
            verify("Added figure is at key '$key'", $this->board->getFigure($key))->same($this->figure);
        });

        $this->specify('Add multiple figures', function() {
            $this->board->addFigure($this->figure, 'pawn1');
            $this->board->addFigure($this->figure2);
            verify('Board contains all added figures', $this->board->getFigures())->count(2);
        });

        $this->specify('Re-insert the same object must be silently ignored', function() {
            $this->board->addFigure($this->figure);
            $this->board->addFigure($this->figure);
            verify('Board contains added figure only once', $this->board->getFigures())->count(1);
        });

        $this->specify('Insert different objects with equal keys must throw exception', function() {
            $this->tester->expectException(new ChessBoardException("Key 'pawn' already in use"), function() {
                $this->board->addFigure($this->figure, 'pawn');
                $this->board->addFigure($this->figure2, 'pawn');
            });
        });

        $this->specify('Add figure with wrong inner coordinates must throw exception', function() {
            $this->tester->expectException(new ChessBoardException('Point out of bounds'), function() {
                $this->board->addFigure(new Pawn(11, 6));
            });
        });
    }

    public function testRemoveFigures()
    {
        $this->board = $board = new ChessBoard(10, 10);
        $this->figure = new Pawn(1, 1);
        $this->figure2 = $figure2 = new Pawn(2, 2);

        $this->board->addFigure($this->figure, 'pawn');
        $this->board->addFigure($this->figure2);

        $this->specify('Removing non existent key must provide no effect', function() {
            $this->board->removeFigureByKey('pawnpawn');
            verify('Figures count stays the same', $this->board->getFigures())->count(2);
        });

        $this->specify('Removing non existent object must provide no effect', function() {
            $this->board->removeFigure(new Pawn(3, 3));
            verify('Figures count stays the same', $this->board->getFigures())->count(2);
        });

        $this->specify('After remove by key figures count must decrease by one', function() {
            $this->board->removeFigureByKey('pawn');
            verify('Board contains one figure', $this->board->getFigures())->count(1);
            verify('Figure on board have index 0', $this->board->getFigure(0))->equals($this->figure2);
        });

        $this->specify('After remove by object figures count must decrease by one', function() use ($board, $figure2) {
            $board->removeFigure($figure2);
            verify('Board contains one figure', $board->getFigures())->count(1);
            verify("Figure on board have key 'pawn'", $board->getFigure('pawn'))->equals($this->figure);
        });
    }

    /**
     * @before testSetBoardSizes
     */
    public function testGetBoardSizes()
    {
        $width = 10;
        $height = 9;
        $board = Stub::construct('app\ChessBoard', ['width' => $width, 'height' => $height], []);

        verify("Returned board width is equal to setted", $board->getWidth())->equals($width);
        verify("Returned board height is equal to setted", $board->getHeight())->equals($height);
    }

    public function testSetBoardSizes()
    {
        $this->specify('Create board with normal sizes', function() {
            $board = new ChessBoard(10, 10);
            verify('Board width equals 10', $board->getWidth())->equals(10);
            verify('Board height equals 10', $board->getHeight())->equals(10);
        });

        $this->specify('Create board with wrong width will throw exception', function() {
            $this->tester->expectException(new ConfigException('Width must be greater then 0'), function() {
                $board = new ChessBoard(-2, 10);
            });
        });

        $this->specify('Create board with wrong height will throw exception', function() {
            $this->tester->expectException(new ConfigException('Height must be greater then 0'), function() {
                $board = new ChessBoard(10, 0);
            });
        });
    }

    public function testMoveFigure()
    {
        $this->board = new ChessBoard(10, 10);
        $this->figure = Stub::construct(
            'app\figures\Pawn',
            ['x' => 1, 'y' => 1],
            [
                'validateMove' => function($x, $y) {
                    return true;
                }
            ]
        );
        $this->figure2 = Stub::construct(
            'app\figures\Pawn',
            ['x' => 2, 'y' => 2],
            [
                'validateMove' => function($x, $y) {
                    return false;
                }
            ]
        );

        $this->specify("Try to move figure that not belongs to the board will throw exception", function() {
            $this->tester->expectException(new ChessBoardException('The figure is not installed on the current board'), function() {
                $this->board->moveFigure($this->figure, 2, 2);
            });
        });

        $this->board->addFigure($this->figure, 'pawn');
        $this->board->addFigure($this->figure2, 'cantMove');

        $this->specify("Try to move out of board bounds will throw exception", function() {
            $this->tester->expectException(new ChessBoardException('Point out of bounds'), function() {
                $figure = $this->board->getFigure('pawn');
                $this->board->moveFigure($figure, 20, 3);
            });
            $this->tester->expectException(new ChessBoardException('Point out of bounds'), function() {
                $figure = $this->board->getFigure('pawn');
                $this->board->moveFigure($figure, 4, -6);
            });
        });

        $this->specify("Move figure to correct coordinates", function() {
            $figure = $this->board->getFigure('pawn');
            $this->board->moveFigure($figure, 5, 5);
        });

        $this->specify("Move figure to incorrect coordinates will throw exception", function() {
            $this->tester->expectException(new ChessBoardException('Invalid move'), function() {
                $figure = $this->board->getFigure('cantMove');
                $this->board->moveFigure($figure, 5, 5);
            });
        });
    }

    public function testCheckBounds()
    {
        $this->board = new ChessBoard(10, 10);

        $this->specify('Validate point in board bounds', function() {
            verify('Point (4,8) is in bounds (10,10)', $this->board->checkBounds(4, 8))->true();
        });

        $this->specify("Validate point with negative coordinates 'x'", function () {
            verify('Point (-4,3) is not in bounds', $this->board->checkBounds(-4, 3))->false();
        });

        $this->specify("Validate point with negative coordinates 'y'", function () {
            verify('Point (4,-3) is not in bounds', $this->board->checkBounds(4, -3))->false();
        });

        $this->specify('Validate point with out of bounds coordinates', function () {
            verify('Point (11,3) is not in bounds', $this->board->checkBounds(11, 3))->false();
        });
    }

    public function testMementoCreateAndLoad()
    {
        $board = new ChessBoard(10, 10);

        $board->addFigure(new Pawn(1, 1), 'pawn1');
        $board->addFigure(new Pawn(2, 2), 'pawn2');

        $memento = $board->createMemento();
        verify('Snapshot is instance of \app\ChessBoardMemento', $memento)->isInstanceOf('\app\ChessBoardMemento');

        $oldBoard = clone $board;
        $board->addFigure(new Pawn(3, 3), 'pawn3');
        verify('Boards are different', $board)->notEquals($oldBoard);

        $board->loadMemento($memento);
        verify('Board state restored', $board)->equals($oldBoard);
    }
}