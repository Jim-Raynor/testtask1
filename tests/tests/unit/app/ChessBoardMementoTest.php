<?php

use app\ChessBoardMemento;
use app\figures\Pawn;

/**
 * Это единственный файл тестов, в котором используется Asserts непосредственно,
 * без обёртки verify. Сделано специально, как один из возможных вариантов.
 */
class ChessBoardMementoTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $figures;
    protected $memento;

    protected function _before()
    {
        $this->figures = [
            new Pawn(1, 1),
            new Pawn(2, 2),
            new Pawn(3, 3),
        ];
        $this->memento = new ChessBoardMemento($this->figures);
    }

    protected function _after()
    {
    }

    public function testCorrectSaveState()
    {
        $savedFigures = $this->memento->getFigures();

        $this->assertEquals($this->figures, $savedFigures, 'Memento creation fails');

        foreach ($this->figures as $figure) {
            $this->assertNotContains($figure, $savedFigures, 'Object must be cloned');
        }
    }

    /**
     * @before testUnserializeRestoresObjectState
     */
    public function testSerializedDataMustBeAString()
    {
        $serial = $this->memento->serialize();
        $this->assertInternalType('string', $serial, 'String expected');
    }

    /**
     * @before testUnserializeFailsWithWrongData
     */
    public function testUnserializeRestoresObjectState()
    {
        $serial = $this->memento->serialize();

        $restoredMemento = new ChessBoardMemento();
        $restoredMemento->unserialize($serial);

        $this->assertEquals($this->memento, $restoredMemento, 'Memento restore fails');
    }

    public function testUnserializeFailsWithWrongData()
    {
        $serial = $this->memento->serialize();

        $restoredMemento = new ChessBoardMemento();
        $restoredMemento->unserialize('222'.$serial);

        $this->assertNotEquals($this->memento, $restoredMemento, 'Memento restored from incorrect data');
    }
}