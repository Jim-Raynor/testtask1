<?php

use app\ChessBoardMemento;
use app\figures\Pawn;
use app\caretaker\FileCaretaker;

class FileCaretakerTest extends \Codeception\Test\Unit
{
    use \Codeception\Specify;

    /**
     * @var \UnitTester
     */
    protected $tester;
    protected $dir = '';
    /**
     * @var \app\caretaker\FileCaretaker
     */
    protected $caretaker;

    protected function _before()
    {
        $this->dir = dirname(dirname(dirname(__DIR__))).DIRECTORY_SEPARATOR.'_output'.DIRECTORY_SEPARATOR.'_FCT_shapshots';

        if (is_dir($this->dir)) {
            $this->fail("Directory '$this->dir' must be deleted before test running");
        }
        if (!mkdir($this->dir)) {
            $this->fail("Can't create directory '$this->dir'");
        }
        $this->caretaker = new FileCaretaker();
        $this->caretaker->setPath($this->dir);
    }

    protected function _after()
    {
        if (!rmdir($this->dir)) {
            $this->fail("Can't remove directory '$this->dir'");
        }
    }

    /**
     * @before testSaveLoadMemento
     */
    public function testGetFileName()
    {
        $key = 'test1';
        $fileName = $this->caretaker->getFileName($key);
        $expectedFilename = $this->dir.DIRECTORY_SEPARATOR."$key.mem";
        verify('Filename is correct', $fileName)->equals($expectedFilename);
    }

    public function testSaveLoadMemento()
    {
        $key = 'test2';
        $memento = new ChessBoardMemento([
            new Pawn(1, 1),
            new Pawn(2, 2),
        ]);

        $this->caretaker->saveMemento($key, $memento);
        $fileName = $this->caretaker->getFileName($key);

        $this->specify('After snapshot saving, file must be created', function() use ($fileName) {
            expect_file('File with saved state exists', $fileName)->exists();
        });

        $this->specify('Loaded snapshot must be exactly the same, as saved', function() use ($key, $memento) {
            $loaded = $this->caretaker->loadMemento($key);
            verify('Loaded object equals saved', $loaded)->equals($memento);
        });

        if (!unlink($fileName)) {
            $this->fail("Can't unlink file '$fileName'");
        }
    }
}