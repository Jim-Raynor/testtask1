<?php
/**
 * Тестовое задание
 * @see https://docs.google.com/document/d/1YsE19WnJjftWjNycPYfDCw8OtbObWekhRV0DaW0y0Xc/edit?pref=2&pli=1
 */
require_once __DIR__.'/../autoload.php';

try {
    echo '<pre>';

    var_dump('Create board');
    $board = new \app\ChessBoard(10, 10);

    // Пробуем добавить фигуры
    var_dump('Add figures');
    $board->addFigure($p1 = new \app\figures\Pawn(1,1), 'p1');
    $board->addFigure($p2 = new \app\figures\Pawn(1,2), 'p2');
    $board->addFigure($q = new \app\figures\Queen(3, 4), 'q');
    $board->addFigure($k = new \app\figures\King(5, 5), 'k');
    print_r($board);

    // Сохраняем/загружаем состояние
    var_dump('Create memento');
    $memento = $board->createMemento();
    print_r($memento);

    var_dump('Move figures');
    $board->moveFigure($k, 6, 5);
    $board->moveFigure($q, 10, 9);
    print_r($board);

    var_dump('Restore from memento');
    $board->loadMemento($memento);
    // Проверим, успешно ли восстановлено состояние
    print_r($board);

    // После загрузки состояния переменные $p1, $p2, $q, $k указывают на объекты,
    // НЕ принадлежащие к доске, так как фигуры являются частью состояния доски
    // и при загрузке состояния все объекты состояния пересоздаются заново.

    // Доступ к фигуре по имени
    var_dump('Figure by key "k"');
    print_r($board->getFigure('k'));

    var_dump('Remove figures "k" and "p1"');
    $board->removeFigureByKey('k');
    $board->removeFigure($board->getFigure('p1'));
    print_r($board);

    try {
        var_dump('Try to do invalid move');
        $board->moveFigure($p2, 4,4);
        print_r($board);
    } catch(\app\ChessBoardException $expected) {
        var_dump('Expected exception: '.$expected->getMessage());
    }

    // Пробуем сохранить/загрузить снимок состояния.
    var_dump('Save memento into storage');
    $memKey = 'test_memento';
    $fileCaretaker = new \app\caretaker\FileCaretaker();
    // указанная директория уже должна существовать и быть доступной для записи
    $fileCaretaker->setPath(__DIR__.'/tmp');
    $fileCaretaker->saveMemento($memKey, $memento);

    var_dump('Load memento from storage');
    $mem = $fileCaretaker->loadMemento($memKey);

    var_dump('Original memento');
    print_r($memento);
    var_dump('Saved/loaded memento');
    print_r($mem);
} catch (\Exception $e) {
    var_dump('UNEXPECTED: '.$e->getMessage());
}