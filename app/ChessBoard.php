<?php
/**
 * Тестовое задание
 * @see https://docs.google.com/document/d/1YsE19WnJjftWjNycPYfDCw8OtbObWekhRV0DaW0y0Xc/edit?pref=2&pli=1
 */

namespace app;

/**
 * Основной класс для представления шахматной доски.
 */
class ChessBoard
{
    /**
     * @var int ширина доски, количество клеток в строке.
     */
    private $width;
    /**
     * @var int длина доски, количество клеток в стобце.
     */
    private $height;
    /**
     * @var array хранит все фигуры доски
     */
    private $figures;

    /**
     * Метод вызывается сразу после добавления фигуры на доску.
     * 
     * @see \app\ChessBoard::addFigure()
     */
    protected function figureAdded()
    {
    }

    /**
     * @param int $width ширина доски, количество клеток. 
     * @param int $height длина доски, количество клеток.
     */
    public function __construct($width, $height)
    {
        $this->figures = [];
        $this->setWidth($width);
        $this->setHeight($height);
    }

    /**
     * Возвращает фигуру по её имени.
     * 
     * @param string $key имя, под которым сохранена фигура.
     * @return \app\ChessFigure|null выбранная фигура, если она существует.
     */
    public function getFigure($key)
    {
        return isset($this->figures[(string)$key]) ? $this->figures[(string)$key] : null;
    }

    /**
     * Возвращает все установленные фигуры в виде массива.
     * 
     * @return \app\ChessFigure[] все фигуры доски.
     */
    public function getFigures()
    {
        return $this->figures;
    }

    /**
     * Добавляет фигуру на доску.
     * 
     * @param \app\ChessFigure $figure добавляемая фигура
     * @param string $key имя, по которому можно будет найти фигуру на доске
     * @throws \app\ChessBoardException
     * @see \app\ChessBoard::getFigure()
     */
    public function addFigure(ChessFigure $figure, $key = null)
    {
        if (!$this->checkBounds($figure->getX(), $figure->getY())) {
            throw new ChessBoardException('Point out of bounds');
        }
        if (!in_array($figure, $this->figures)) {
            if ($key === null) {
                $this->figures[] = $figure;
            } elseif (!isset($this->figures[$key])) {
                $this->figures[$key] = $figure;
            } else {
                throw new ChessBoardException("Key '$key' already in use");
            }
            $this->figureAdded();
        }
    }

    /**
     * Передвигает фигуру на доске.
     * 
     * @param \app\ChessFigure $figure фигура, которую нужно переместить
     * @param int $x новая координата фигуры (номер столбца)
     * @param int $y новая координата фигуры (номер строки)
     * @throws \app\ChessBoardException
     */
    public function moveFigure(ChessFigure $figure, $x, $y)
    {
        if (!in_array($figure, $this->figures)) {
            throw new ChessBoardException('The figure is not installed on the current board');
        }
        if (!$this->checkBounds($x, $y)) {
            throw new ChessBoardException('Point out of bounds');
        }
        if (!$figure->move($x, $y)) {
            throw new ChessBoardException('Invalid move');
        }
    }

    /**
     * Удаляет фигуру с доски по имени ключа.
     * 
     * @param string|int $key ключ фигуры
     */
    public function removeFigureByKey($key)
    {
        if (is_scalar($key)) {
            unset($this->figures[$key]);
        }
    }

    /**
     * Удаляет фигуру с доски.
     * 
     * @param string|int $key ключ фигуры
     */
    public function removeFigure(ChessFigure $figure)
    {
        if (false !== ($key = array_search($figure, $this->figures, true))) {
            unset($this->figures[$key]);
        }
    }

    /**
     * Проверяет, принадлежит ли клетка с указанными координатами текущей доске.
     * 
     * @param int $x номер столбца 
     * @param int $y номер строки
     * @return boolean TRUE - координата принадлежит доске, иначе - FALSE.
     */
    public function checkBounds($x, $y)
    {
        return ($x > 0 && $x <= $this->width && $y > 0 && $y <= $this->height);
    }

    /**
     * Создаёт и возвращает объект, хранящий текущее состояние доски.
     * 
     * @return \app\ChessBoardMemento
     */
    public function createMemento()
    {
        return new ChessBoardMemento($this->figures);
    }

    /**
     * Восстанавливает состояние системы из сохранённого снимка.
     * 
     * @param \app\ChessBoardMemento $memento
     */
    public function loadMemento(ChessBoardMemento $memento)
    {
        $this->figures = $memento->getFigures();
    }

    /**
     * Возвращает количество клеток в строке.
     * 
     * @return int ширина доски, количество клеток в строке.
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Возвращает количество клеток в стобце.
     * 
     * @return int длина доски, количество клеток в стобце.
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Устанавливает ширину доски.
     * 
     * @param int $width ширина доски, количество клеток в строке.
     * @throws \app\ConfigException
     */
    private function setWidth($width)
    {
        if (!is_numeric($width) || (int)$width < 1) {
            throw new ConfigException('Width must be greater then 0');
        }

        $this->width = (int)$width;
    }

    /**
     * Устанавливает длину доски.
     * 
     * @param int $height длина доски, количество клеток в стобце.
     * @throws \app\ConfigException
     */
    private function setHeight($height)
    {
        if (!is_numeric($height) || (int)$height < 1) {
            throw new ConfigException('Height must be greater then 0');
        }

        $this->height = (int)$height;
    }
}