<?php
/**
 * Тестовое задание
 * @see https://docs.google.com/document/d/1YsE19WnJjftWjNycPYfDCw8OtbObWekhRV0DaW0y0Xc/edit?pref=2&pli=1
 */

namespace app;

/**
 * Основной класс для представления шахматной фигуры.
 */
abstract class ChessFigure
{
    /**
     * @var int координата фигуры, номер столбца на доске.
     */
    private $x;
    /**
     * @var int координата фигуры, номер строки на доске.
     */
    private $y;

    /**
     * @param int $x номер столбца на доске.
     * @param int $y номер строки на доске.
     */
    public function __construct($x, $y)
    {
        $this->x = (int)$x;
        $this->y = (int)$y;
    }

    /**
     * Возвращает название фигуры.
     * 
     * @return string название фигуры
     */
    abstract public static function getName();

    /**
     * Проверяет корректность хода.
     * 
     * @param int $x новый номер столбца
     * @param int $y новый номер строки
     * @return boolean возможено ли перемещение в новую клетку. TRUE - если возможно, иначе - FALSE.
     */
    abstract public function validateMove($x, $y);

    /**
     * Устанавливает новые координаты фигуре (перемещает её).
     * 
     * @param int $x новая координата фигуры (по ширине доски)
     * @param int $y новая координата фигуры (по длине доски)
     * @return boolean TRUE - если перемещение успешно, иначе - FALSE.
     */
    public function move($x, $y)
    {
        if ($this->validateMove($x, $y)) {
            $this->x = $x;
            $this->y = $y;
            return true;
        }
        return false;
    }

    /**
     * Возвращает координату X.
     * 
     * @return int координата X, номер столбца на доске.
     */
    public function getX()
    {
        return $this->x;
    }

    /**
     * Возвращает координату Y.
     * 
     * @return int координата Y, номер строки на доске.
     */
    public function getY()
    {
        return $this->y;
    }
}