<?php
/**
 * Тестовое задание
 * @see https://docs.google.com/document/d/1YsE19WnJjftWjNycPYfDCw8OtbObWekhRV0DaW0y0Xc/edit?pref=2&pli=1
 */

namespace app;

/**
 * Интерфейс, описывающий методы для сохранения/загрузки состояния шахматной доски в различных хранилищах.
 */
interface IChessBoardCaretaker
{
    /**
     * Сохраняет снимок состояния в хранилище.
     * 
     * @param mixed $key ключ, по которому нужно сохранить снимок.
     * @param \app\ChessBoardMemento $memento снимок состояния
     * @return boolean TRUE - сохранение прошло успешно, иначе - FALSE.
     */
    public function saveMemento($key, ChessBoardMemento $memento);

    /**
     * Возвращает снимок состояния из хранилища.
     * 
     * @param mixed $key ключ, по которому нужно искать снимок.
     * @return \app\ChessBoardMemento|null объект состояния, либо NULL, если снимок не найден.
     */
    public function loadMemento($key);
}