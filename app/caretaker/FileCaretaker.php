<?php
/**
 * Тестовое задание
 * @see https://docs.google.com/document/d/1YsE19WnJjftWjNycPYfDCw8OtbObWekhRV0DaW0y0Xc/edit?pref=2&pli=1
 */

namespace app\caretaker;

use app\ChessBoardMemento;
use app\IChessBoardCaretaker;

/**
 * Класс для сохранения/загрузки состояний доски в файлах.
 * 
 * Функционал очень приблизительный, пропущены всевозможные проверки на доступность путей,
 * существование файла и т.п.
 */
class FileCaretaker implements IChessBoardCaretaker
{
    /**
     * @var string путь к файлу со снимком состояния
     */
    private $path;

    /**
     * @inheritdoc
     */
    public function saveMemento($key, ChessBoardMemento $memento)
    {
        return (boolean)file_put_contents($this->getFileName($key), $memento->serialize(), FILE_BINARY);
    }

    /**
     * @inheritdoc
     */
    public function loadMemento($key)
    {
        $data = file_get_contents($this->getFileName($key));
        if ($data !== null) {
            $memento = new ChessBoardMemento();
            $memento->unserialize($data);
            return $memento;
        }
    }

    /**
     * Устанавливает путь.
     * 
     * @param string $path путь к файлу со снимком состояния.
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Возвращает имя файла для ключа.
     * 
     * @param string $key ключ
     * @return string имя файла
     */
    public function getFileName($key)
    {
        return rtrim($this->path, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR.strtr($key, DIRECTORY_SEPARATOR, '').'.mem';
    }
}