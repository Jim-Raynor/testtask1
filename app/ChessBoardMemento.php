<?php
/**
 * Тестовое задание
 * @see https://docs.google.com/document/d/1YsE19WnJjftWjNycPYfDCw8OtbObWekhRV0DaW0y0Xc/edit?pref=2&pli=1
 */

namespace app;

/**
 * Класс для хранения состояния шахматной доски.
 */
class ChessBoardMemento implements \Serializable
{
    /**
     * @var array хранилище с фигурами доски
     */
    private $figures;

    /**
     * @param array $figures
     */
    public function __construct(array $figures = [])
    {
        // Чтобы не описывать вручную клонирование всех атрибутов и их иерархий
        // используем сериализацию/десериализацию всей коллекции.
        $this->figures = unserialize(serialize($figures));
    }

    /**
     * Возвращает фигуры доски.
     * 
     * @return array
     */
    public function getFigures()
    {
        return $this->figures;
    }

    /**
     * @inheritdoc
     */
    public function serialize()
    {
        return serialize([
            'figures' => $this->figures
        ]);
    }

    /**
     * @inheritdoc
     */
    public function unserialize($serialized)
    {
        $data = @unserialize($serialized);
        $this->figures = $data['figures'];
    }
}