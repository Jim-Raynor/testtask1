<?php
/**
 * Тестовое задание
 * @see https://docs.google.com/document/d/1YsE19WnJjftWjNycPYfDCw8OtbObWekhRV0DaW0y0Xc/edit?pref=2&pli=1
 */

namespace app\figures;

use app\ChessFigure;

/**
 * Пешка.
 */
class Pawn extends ChessFigure
{
    /**
     * @inheritdoc
     */
    public static function getName()
    {
        return 'Pawn';
    }

    /**
     * @inheritdoc
     */
    public function validateMove($x, $y)
    {
        return (abs($this->getX() - $x) <= 1 && abs($this->getY() - $y) <= 1);
    }
}