<?php
/**
 * Тестовое задание
 * @see https://docs.google.com/document/d/1YsE19WnJjftWjNycPYfDCw8OtbObWekhRV0DaW0y0Xc/edit?pref=2&pli=1
 */

namespace app;

/**
 * Класс исключения при некорректном обращении с доской.
 */
class ChessBoardException extends Exception
{
}